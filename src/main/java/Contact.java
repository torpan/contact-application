public class Contact
{
    public String fullName;
    public String Address;
    public String mobileNumber;
    public String workNumber;
    public String birthDate;

    public Contact(String fullName, String Address, String mobileNumber, String workNumber, String birthDate)
    {
        this.fullName = fullName;
        this.Address = Address;
        this.mobileNumber = mobileNumber;
        this.workNumber = workNumber;
        this.birthDate = birthDate;
    }

    public String getName()
    {
        return fullName;
    }
    public String getMobileNumber()
    {
        return mobileNumber;
    }
    public String getWorkNumber()
    {
        return workNumber;
    }


}
