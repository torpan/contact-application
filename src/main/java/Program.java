import java.util.*;

public class Program
{
    public static void main(String[] args)
    {
        ArrayList<Contact> contactList = new ArrayList<>();

        contactList.add(new Contact("Tora Haukka", "Faglatorpet", "0768462140", "-", "950215"));
        contactList.add(new Contact("Bruno Haukka", "Storgatan 16", "0709123456", "044101752", "620814"));
        contactList.add(new Contact("Anette Haukka", "Storgatan 16", "0709345678", "0809123465", "650113"));
        contactList.add(new Contact("Cecilia Simonsson", "Fredrikbergsgatan 12", "0709103399", "-", "940412"));
        contactList.add(new Contact("Madeleine Hentzel", "Mesgatan 12", "0709567432", "-", "940610"));
        contactList.add(new Contact("Astrid Haukka", "Vapengatan 18", "0709944745", "-", "930102"));
        contactList.add(new Contact("Klas Lind", "Vapengatan 18", "0709987645", "-", "920912"));
        contactList.add(new Contact("Eric Haukka", "Studentgatan 23", "0709562312", "-", "950215"));
        contactList.add(new Contact("Frida Karlander", "Apotekare Reinholds gata 18", "0709321456", " ","931210"));
        contactList.add(new Contact("John Iversen", "Lundagatan 20", "0709876345", "0809123421", "941119"));

        //Scanner to read input from the user.
        Scanner sc = new Scanner(System.in);

        System.out.println("Please enter what you want to search for.");
        String userInput = sc.next();

        for(Contact contact : contactList)
        {
            if (contact.fullName.contains(userInput) || contact.Address.contains(userInput) || contact.mobileNumber.contains(userInput) || contact.workNumber.contains(userInput) || contact.birthDate.contains(userInput))
            {
                System.out.println("Name: " + contact.fullName+" "+"Address: "+contact.Address+" "+"Mobile number: "+contact.mobileNumber+" "+"Work number: "+contact.workNumber+" "+"Birth date: "+contact.birthDate);
            }

        }

    }
}
